build:
	sudo docker build -t "lavalink:latest" .

start:
	sudo docker run -v $(pwd)/lavalink.yaml:/opt/lavalink/application.yaml -p 2333:2333 --name lavalink "lavalink:latest"

startbkg:
	sudo docker run -d -v $(pwd)/lavalink.yaml:/opt/lavalink/application.yaml -p 2333:2333 --name lavalink "lavalink:latest"

logs:
	sudo docker logs -t -f lavalink

stop:
	sudo docker container stop lavalink

remove:
	sudo docker container rm lavalink
