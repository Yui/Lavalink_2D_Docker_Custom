# Lavalink

## Config

- Create a copy of file `lavalink-template.yaml` with name `lavalink.yaml` and configure it.

## Build

`make` (On windows use the powershell script.)

## Run

`make start` (On windows use the powershell script.)
