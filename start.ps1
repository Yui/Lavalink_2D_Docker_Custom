function Show-Menu {
    param (
        [string]$Title = 'Lavalink Menu'
    )
    Clear-Host
    Write-Host "================ $Title ================"

    Write-Host "1: Press '1' to build the docker image."
    Write-Host "2: Press '2' to run the docker image (Image must already be built)."
    Write-Host "3: Press '3' to run the docker container in background (Image must already be built)."
    Write-Host "4: Press '4' to see the logs of the container(Container must be running)."
    Write-Host "5: Press '5' to stop the container(Container must be running)."
    Write-Host "6: Press '6' to remove the container(Container must be stopped)."
    Write-Host "Q: Press 'Q' to quit."
}

do
 {
    Show-Menu
    $selection = Read-Host "Please make a selection"
    switch ($selection)
    {
    '1' {
    docker build -t "lavalink:latest" .
    } '2' {
    docker run -v ${pwd}/lavalink.yaml:/opt/lavalink/application.yaml -p 2333:2333 --name lavalink "lavalink:latest"
    } '3' {
    docker run -d -v ${pwd}/lavalink.yaml:/opt/lavalink/application.yaml -p 2333:2333 --name lavalink "lavalink:latest"
    } '4' {
    docker logs -t -f lavalink
    } '5' {
    docker container stop lavalink
    } '6' {
    docker container rm lavalink
    }
    }
    pause
 }
 until ($selection -eq 'q')